/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.util;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 * Used for making API calls
 * Going to make a "google search bar" for the background.
 * 
 * @author Alex
 */
public class ApiUtil {
    
    public static Image getFromUrl(String url) {
        try {
            return ImageIO.read(new URL(url));
        } catch(IOException e) {
            
        }
        return null;
    }
    
}
