package elguezabal3;

import elguezabal3.data.DataManager;
import elguezabal3.data.GraphManager;
import elguezabal3.gui.Frame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Alex
 */
public class Main {
    
    private static Frame frame;   
    
    private static DataManager dataManager;
    
    private static GraphManager graphManager;
    
    public static void main(String[] args) {        
        // Managers
        // Creates the data manager
        dataManager = new DataManager();
               
        // Greates the graph manager
        graphManager = new GraphManager();
        
        // Opens the frame
        SwingUtilities.invokeLater(() -> {
            frame = new Frame();
        });
    }

    /**
     * @return the frame
     */
    public static Frame getFrame() {
        return frame;
    }

    /**
     * @return the dataManager
     */
    public static DataManager getDataManager() {
        return dataManager;
    }

    /**
     * @return the graphManager
     */
    public static GraphManager getGraphManager() {
        return graphManager;
    }
    
}
