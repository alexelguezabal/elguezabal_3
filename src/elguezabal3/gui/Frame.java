/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui;

import elguezabal3.gui.panels.GraphPanel;
import elguezabal3.gui.panels.SelectionPanel;
import elguezabal3.gui.panels.SettingsPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;


/**
 * The GUI builder class.
 *
 * @author Alex
 */
public class Frame extends JFrame {

    // GUI Specifications
    private final int GUI_WIDTH = 1220;
    private final int GUI_HEIGHT = 970;

    private static int GUI_X_BOUNDS;
    private static int GUI_Y_BOUNDS;
        
    // Panels
    private SelectionPanel selectionPanel;
    private GraphPanel graphPanel;
    private SettingsPanel settingsPanel;
    
    public Frame() {
        super("Graph");

        //Autoclose
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        //Size
        this.setSize(GUI_WIDTH, GUI_HEIGHT);

        // Prefered Size
        this.setPreferredSize(new Dimension(GUI_WIDTH, GUI_HEIGHT));
        
        //Resizeable
        this.setResizable(false);

        // Sets the layout
        this.setLayout(new BorderLayout());

        // Sets the icon
        Image icon = null;
        try {
            icon = ImageIO.read(new File("icon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        // Image must exist
        if (icon != null) {
            this.setIconImage(icon);
        }
        
        // Allows this frame to be focusable
        setFocusable(true);

        // Packs the frame
        pack();

        // Adds variables
        init();

        // Displayer
        this.setVisible(true);

        // Used for loading in objects after the gui is built
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
            @Override
            public void run() {
                
            }
        }, 500);
    }

    /**
     * Init Methods
     */
    private void init() {
        // Do listeners first, so all panels can have the listeners
        initListeners();
        initPanels();
    }

    /*
    Initizlies all JPanels
     */
    private void initPanels() {        
        this.selectionPanel = new SelectionPanel(this);
        this.graphPanel = new GraphPanel(this);
        this.settingsPanel = new SettingsPanel(this);
    }
    
    /**
     * Initilizes all listeners
     */
    private void initListeners() {
    }

    /**
     * @return the selectionPanel
     */
    public SelectionPanel getSelectionPanel() {
        return selectionPanel;
    }

    /**
     * @return the graphPanel
     */
    public GraphPanel getGraphPanel() {
        return graphPanel;
    }

    /**
     * @return the settingsPanel
     */
    public SettingsPanel getSettingsPanel() {
        return settingsPanel;
    }
}