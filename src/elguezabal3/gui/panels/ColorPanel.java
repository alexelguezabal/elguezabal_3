/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.panels;

import elguezabal3.Main;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import javax.swing.JFrame;

/**
 * Class to display the color pallet image
 * 
 * @author Alex
 */
public class ColorPanel extends APanel<ColorPanel> {

    private Image image;
    
    public ColorPanel(JFrame frame, Image image, int width, int height) {
        super(frame);
        this.image = image;
        setSize(new Dimension(width, height));
        
        // Listeners
        addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent evt) {
          // Current Mouse Location
          Point point = MouseInfo.getPointerInfo().getLocation();

           Color color = null;
           try {
                    // Color at Location
                    color = getColorAt(get().getFrame(), point);
           } catch (ArrayIndexOutOfBoundsException e) {

           }

           // Updates the Color of the current Tool.
           if (color != null) {
               Main.getGraphManager().setCurrentColor(color);
           }

            // Closes the frame
           get().getFrame().setVisible(false);

           // Updates the current color
           Main.getGraphManager().updateColorOfGraph();
         }
        });
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(image, WIDTH-1, HEIGHT-1, this);
    }

    @Override
    public ColorPanel get() {
        return this;
    }
    
        /**
     * !(https://stackoverflow.com/questions/60906929/java-swing-how-to-get-the-color-of-a-pixel-of-a-jframe)
     * Gets the color at a certian point
     * Used for figuring out what color is selected
     * 
     * @param frm Frame to chose from
     * @param p Point to look for
     * @return Color at the perspective point
     */
    public static Color getColorAt(JFrame frm, Point p) {
        Rectangle rect = frm.getContentPane().getBounds();
        BufferedImage img = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_ARGB);
        frm.getContentPane().paintAll(img.createGraphics());
        return new Color(img.getRGB(p.x, p.y), true);
    }
    
}
