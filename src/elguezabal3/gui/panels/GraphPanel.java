/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.panels;

import elguezabal3.Main;
import elguezabal3.gui.Frame;
import elguezabal3.gui.listeners.MouseListener;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author Alex
 */
public class GraphPanel extends APanel<GraphPanel> {

    /**
     * Default Constructor
     * 
     * @param frame 
     */
    public GraphPanel(Frame frame) {
        super(frame, BorderLayout.CENTER);
        
        init();
    }
    
    private void init() {
        addMouseListener(new MouseListener());
    }

    @Override
    public GraphPanel get() {
        return this;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        g.drawRect(0, 0, getWidth()-1, getHeight()-1);
        
        Graphics2D graphics2D = (Graphics2D) g;
        
        Main.getGraphManager().getCurrentGraphToDisplay().getjFreeChart().draw(graphics2D, new Rectangle2D.Double(1, 1, getWidth()-2, getHeight()-2));
    }
    
    public void displayGraph() {
        repaint();
    }
       
}
