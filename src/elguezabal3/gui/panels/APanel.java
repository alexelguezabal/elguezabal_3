/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.panels;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Abstract wrapper for all panels
 * @author Alex
 * @param <T> The sub class.
 */
public abstract class APanel<T extends APanel> extends JPanel {
    
    private JFrame frame;
    private String borderLayout;
    
    
    public APanel(JFrame frame) {
        this.frame = frame;
        
        // Makes this component focusable
        this.setFocusable(true);
        
        // Adds this panel to the frame.
        frame.add(this);
    }
    
    /**
     * Default constructor
     * 
     * @param frame Super frame of this APanel.
     * @param layout Border Layout position of this Component
     */
    public APanel(JFrame frame, String layout) {
        this.frame = frame;
        
        // Makes this component focusable
        this.setFocusable(true);
        
        this.borderLayout = layout;
        
        // Adds this panel to the frame.
        frame.add(this, layout);
    }
    
    /**
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }
    
    public abstract T get();
        
}