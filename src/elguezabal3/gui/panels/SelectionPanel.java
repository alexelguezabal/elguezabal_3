/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.panels;

import elguezabal3.Main;
import elguezabal3.data.Info;
import elguezabal3.gui.Frame;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 *
 * @author Alex
 */
public class SelectionPanel extends APanel<SelectionPanel> implements ItemListener {

    private List<String> isoCodes;
    private JComboBox<Object> currentCountryBox;
    private JComboBox<Object> currentInfoBox;
    
    private JLabel header;
    private JLabel boxLabel;
    private JLabel informationLabel;
    
    private JButton saveImage;
    
    /**
     * Default constructor
     * 
     * @param frame 
     */
    public SelectionPanel(Frame frame) {
        super(frame, BorderLayout.NORTH);
        
        setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        
        try {
            init();
        } catch (IOException e) {

        }
    }
    
    /**
     * Initilizes this panels attributes
     */
    private void init() throws IOException {
        // List of all the countrys suppored
        this.isoCodes = Main.getDataManager().getPoints().keySet().stream().sorted().collect(Collectors.toList());
        
        // Box of the countrys suppored
        this.currentCountryBox = new JComboBox<>(isoCodes.toArray());
        
        // Other Components
        ImageIcon headerIcon = new ImageIcon(ImageIO.read(new File("covid-19-information-2_0.png")).getScaledInstance(250, 75, Image.SCALE_DEFAULT));
        this.header = new JLabel(headerIcon);
        
        this.boxLabel = new JLabel(" Select a Country: ");
        this.boxLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        
        this.informationLabel = new JLabel("   Select Information: ");
        this.informationLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        
        // Box of the current information
        this.currentInfoBox = new JComboBox<>(Info.boxNames());
                
        // Listeners
        this.currentCountryBox.addItemListener(this);
        this.currentInfoBox.addItemListener(this);
        
        this.saveImage = new JButton("Save Graph");
        this.saveImage.addActionListener((ActionEvent e) -> { Main.getGraphManager().saveCurrentGraph(); });
        
        // Adds to the panel
        
        add(header);
        add(new JLabel("    "));
        add(saveImage);
        add(new JLabel("  "));
        add(boxLabel);
        add(currentCountryBox);
        add(informationLabel);
        add(currentInfoBox);
    }
        
    @Override
    public void paintComponent(Graphics g) {
        g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }

    @Override
    public SelectionPanel get() {
        return this;
    }  

    /**
     * Listener for the selection boxes in this object
     * Updates the current graph based on the current country (iso code) and Info type.
     * @param e Event
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        // Updates the graph with the according information and country
        Main.getGraphManager().updateCurrentGraph(Main.getGraphManager().createGraph(
                Main.getDataManager().getPoints((String) currentCountryBox.getSelectedItem()), 
                Info.getByName((String) currentInfoBox.getSelectedItem()))
        );
        
        // Updates the domain Slider
        Main.getFrame().getSettingsPanel().updateRangeSlider(true);
        
        Main.getFrame().getGraphPanel().repaint();
    }
       
}
