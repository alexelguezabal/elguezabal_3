package elguezabal3.gui.panels;

import com.jidesoft.swing.RangeSlider;
import elguezabal3.Main;
import elguezabal3.data.DataPoint;
import elguezabal3.data.Graph;
import elguezabal3.gui.Frame;
import elguezabal3.gui.frames.ColorPalleteFrame;
import elguezabal3.gui.listeners.SearchBarListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Alex
 */
public class SettingsPanel extends APanel<SettingsPanel> {

    private JPanel topPanel;
    private JPanel bottomPanel;
    
    private RangeSlider domainSlider;
    private String bottomDate;
    private String topDate;
    
    private RangeSlider rangeSlider; 
    private double bottomBounds;
    private double topBounds;
    private JLabel bottomBoundsLabel;
    private JLabel topBoundsLabel;
       
    private JLabel currentColorLabel;
    
    private JLabel domainLabel;
    private JLabel colorLabel;
    private JLabel backgroundSearchLabel;
    private JLabel rangeLabel;
    
    private JTextField backgroundSearch;
    
    private JButton quitButton;
    private JLabel spaceLabel;
    
    /**
     * Default Constructor
     * 
     * @param frame 
     */
    public SettingsPanel(Frame frame) {
        super(frame, BorderLayout.SOUTH);
        
        setLayout(new GridLayout(2, 1, 10, 10));
        
        init();
    }
    
    private void init() {
        // Inititiates both the top and bottom settings panels
        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));

        
        // Using the current graph to update the panels 
        Graph currentGraph = Main.getGraphManager().getCurrentGraph();
        
        DataPoint bottomPoint = currentGraph.getPoints().get(0);
        DataPoint topPoint = currentGraph.getPoints().get(currentGraph.getPoints().size()-1);

        this.bottomDate = bottomPoint.getDate();
        this.topDate = topPoint.getDate();
                
        // Creates the range slider
        this.domainSlider = new RangeSlider(0, currentGraph.getPoints().size(), 0, currentGraph.getPoints().size());
        
        // Whenever the slider is moved, the graph will be updated accordinly.
        this.domainSlider.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent e) {                                
                try {
                    Main.getGraphManager().displayTemporaryGraph(Main.getGraphManager().getCurrentGraph().getPoints().subList(domainSlider.getLowValue(), domainSlider.getHighValue()),
                            Main.getGraphManager().getCurrentGraph().getInfo(),
                            Main.getGraphManager().getCurrentGraph().getColor());
                } catch (IndexOutOfBoundsException me) {

                }
            }
            
        });

        // Adds the rangeSlider listener
        // Whenever the slider is moved, the graph will be updated accordinly.
        /*
        this.domainSlider.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent e) {                                
                try {
                    Main.getGraphManager().displayTemporaryGraph(Main.getGraphManager().getCurrentGraph().getPoints().subList(domainSlider.getLowValue(), domainSlider.getHighValue()),
                            Main.getGraphManager().getCurrentGraph().getInfo(),
                            Main.getGraphManager().getCurrentGraph().getColor());
                } catch (IndexOutOfBoundsException me) {

                }
            }
            
        });
        */
        
        // Labels
        this.domainLabel = new JLabel("Select a Date Range: ");
        this.domainLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        
        this.colorLabel = new JLabel("Select a Color: ");
        this.colorLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        
        this.backgroundSearchLabel = new JLabel("Enter a Background Image URL: ");
        this.backgroundSearchLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        
        // Assigns the default value to the current color label.
        updateCurrentColorLabel(false);
        
        // Creating the text field for searching
        this.backgroundSearch = new JTextField("Enter a url:");
        this.backgroundSearch.setColumns(15);
        
        // Search Text field labels
        this.backgroundSearch.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                backgroundSearch.setText("");
            }
        });
        this.backgroundSearch.addKeyListener(new SearchBarListener());
        
        // Bottom Panel Initilization
        
        // Quit button
        this.quitButton = new JButton("Quit");
        // Quits and exits the program 
        this.quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(-1);
            }
        });
    
        updateRangeSlider(false);
        this.spaceLabel = new JLabel("   ");
        
        // Top Panel
        this.topPanel.add(domainLabel);
        this.topPanel.add(new JLabel(bottomDate));
        this.topPanel.add(domainSlider);
        this.topPanel.add(new JLabel(topDate));
        this.topPanel.add(new JLabel("   "));
        this.topPanel.add(backgroundSearchLabel);
        this.topPanel.add(backgroundSearch);
        this.topPanel.add(new JLabel("   "));
        this.topPanel.add(colorLabel);
        this.topPanel.add(currentColorLabel);
        
        // Bottom Panel
        this.bottomPanel.add(rangeLabel);
        this.bottomPanel.add(bottomBoundsLabel);
        this.bottomPanel.add(rangeSlider);
        this.bottomPanel.add(topBoundsLabel);
        this.bottomPanel.add(spaceLabel);
        this.bottomPanel.add(quitButton);
                
        add(topPanel);
        add(bottomPanel);
    }
    
    /**
     * Updates the range slider with the update of the graph
     * Called when the info of the graph is changed or the country graph
     * Ex. when the full graph is changed, not modified.
     * 
     * @param update If the UI should be updated or not
     */
    public void updateRangeSlider(boolean update) {
        if(update) {
            this.bottomPanel.remove(rangeLabel);
            this.bottomPanel.remove(bottomBoundsLabel);
            this.bottomPanel.remove(rangeSlider);
            this.bottomPanel.remove(topBoundsLabel);
            this.bottomPanel.remove(spaceLabel);
            this.bottomPanel.remove(quitButton);
        }
        
        // Using the current graph to update the panels 
        Graph currentGraph = Main.getGraphManager().getCurrentGraph();
        
        DataPoint bottomPoint = currentGraph.getPoints().get(0);
        DataPoint topPoint = currentGraph.getPoints().get(currentGraph.getPoints().size()-1);
        
        this.bottomBounds = bottomPoint.getFromInfo(currentGraph.getInfo());
        this.topBounds = topPoint.getFromInfo(currentGraph.getInfo());
        
        this.rangeSlider = new RangeSlider(0, currentGraph.getPoints().size(), 0, currentGraph.getPoints().size());
        
        // Whenever the slider is moved, the graph will be updated accordinly.
        this.rangeSlider.addChangeListener(new ChangeListener() {
            
            @Override
            public void stateChanged(ChangeEvent e) {                                
                try {
                    Main.getGraphManager().displayTemporaryGraph(Main.getGraphManager().getCurrentGraph().getPoints().subList(rangeSlider.getLowValue(), rangeSlider.getHighValue()),
                            Main.getGraphManager().getCurrentGraph().getInfo(),
                            Main.getGraphManager().getCurrentGraph().getColor());
                } catch (IndexOutOfBoundsException me) {

                }
            }
            
        });
        
        // Creates the labels
        this.rangeLabel = new JLabel(String.format("Slect a %s Range: ", currentGraph.getInfo().getBoxSelectionName()));
        
        this.bottomBoundsLabel = new JLabel(""+bottomBounds);
        this.bottomBoundsLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));

        this.topBoundsLabel = new JLabel(""+topBounds);
        this.topBoundsLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));

        
        // Adds components to bottom panel
        if(update) {
            this.bottomPanel.add(rangeLabel);
            this.bottomPanel.add(bottomBoundsLabel);
            this.bottomPanel.add(rangeSlider);
            this.bottomPanel.add(topBoundsLabel);
            this.bottomPanel.add(spaceLabel);
            this.bottomPanel.add(quitButton);
            revalidate();
        }
        
    }
    
    /**
     * Called when the current color label is to be updated
     * 
     * @param updateScreen Determines if this color will be updated, used for first use.
     */
    public void updateCurrentColorLabel(boolean updateScreen) {
        
        // If it doesn't already exist
        if(this.currentColorLabel != null && updateScreen)
            this.topPanel.remove(this.currentColorLabel);
        
        // Creates the new icon
        BufferedImage img = new BufferedImage(35, 35, BufferedImage.TYPE_INT_ARGB);
        Graphics g = img.getGraphics();
        g.setColor(Main.getGraphManager().getCurrentColor());
        g.fillRect(1, 1, img.getWidth()-2, img.getHeight()-2);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, img.getWidth()-1, img.getWidth()-1);
        g.dispose();
        
        // Updates the {@code currentColorLabel} JLabel instance
        this.currentColorLabel = new JLabel(new ImageIcon(img));
        
        // Called when this label is clicked, opening the painting GUI.
        this.currentColorLabel.addMouseListener(new MouseAdapter() {          
            @Override
            public void mouseClicked(MouseEvent e) {
                SwingUtilities.invokeLater(() -> new ColorPalleteFrame());
            }
        });
        
        // Updates the frame acordingly, with the new JLabel.
        if(updateScreen) {
            this.topPanel.add(this.currentColorLabel);
            revalidate();
        }
    }

    @Override
    public SettingsPanel get() {
        return this;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }
}
