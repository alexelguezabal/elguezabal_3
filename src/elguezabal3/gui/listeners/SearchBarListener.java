/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.listeners;

import elguezabal3.Main;
import elguezabal3.util.ApiUtil;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;

/**
 *
 * @author Alex
 */
public class SearchBarListener implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(!(e.getSource() instanceof JTextField)) return;
        
        // Enter has been pressed
        if(e.getKeyCode() == 10) {
            
            // Gets the text in the field and clears it.
            JTextField jTextField = (JTextField) e.getSource();
            
            final String url = jTextField.getText();
            
            jTextField.setText("");
            
            // Gets the image, if it is null, return
            Image image = ApiUtil.getFromUrl(url);
            
            if(image == null) return;
            
            // Updates the current background image
            Main.getGraphManager().setCurrentBackgroundImage(image);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
}
