/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.gui.listeners;

import elguezabal3.Main;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Alex
 */
public class MouseListener extends MouseAdapter {
    
    @Override
    public void mouseClicked(MouseEvent e) {
        Main.getGraphManager().getCurrentGraph().getToolTip(e.getX(), e.getY());
    }
    
}
