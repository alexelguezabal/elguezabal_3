package elguezabal3.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Alex
 */
public class DataManager {
    //              0       1           2       3       4           5       6                   7           8           9                   10                      11                      12                              13                      14                  15                              16                  17          18                              19         20                           21             22                               23                      24                                  25      26              27                      28                  29                  30                              31              32          33          34                  35                  36                  37              38              39                          40                                  41                      42                                      43                          44                                      45                                                    46           47                   48             49             50        51          52              53          54              55                      56                  57            58            59                  60                          61              62                      63                                      64                          65              66                                                                                                                                               
    // Split Info : iso_code,continent,location,date,total_cases,new_cases,new_cases_smoothed,total_deaths,new_deaths,new_deaths_smoothed,total_cases_per_million,new_cases_per_million,new_cases_smoothed_per_million,total_deaths_per_million,new_deaths_per_million,new_deaths_smoothed_per_million,reproduction_rate,icu_patients,icu_patients_per_million,hosp_patients,hosp_patients_per_million,weekly_icu_admissions,weekly_icu_admissions_per_million,weekly_hosp_admissions,weekly_hosp_admissions_per_million,new_tests,total_tests,total_tests_per_thousand,new_tests_per_thousand,new_tests_smoothed,new_tests_smoothed_per_thousand,positive_rate,tests_per_case,tests_units,total_vaccinations,people_vaccinated,people_fully_vaccinated,total_boosters,new_vaccinations,new_vaccinations_smoothed,total_vaccinations_per_hundred,people_vaccinated_per_hundred,people_fully_vaccinated_per_hundred,total_boosters_per_hundred,new_vaccinations_smoothed_per_million,new_people_vaccinated_smoothed,new_people_vaccinated_smoothed_per_hundred,stringency_index,population,population_density,median_age,aged_65_older,aged_70_older,gdp_per_capita,extreme_poverty,cardiovasc_death_rate,diabetes_prevalence,female_smokers,male_smokers,handwashing_facilities,hospital_beds_per_thousand,life_expectancy,human_development_index,excess_mortality_cumulative_absolute,excess_mortality_cumulative,excess_mortality,excess_mortality_cumulative_per_million
   
    private final String dataFilePath = "owid-covid-data.csv";
    public HashMap<String, ArrayList<DataPoint>> points;
    
    public DataManager() {
        
        // Initilizes the points map
        this.points = new HashMap<>();
        
        // Loads the file
        loadFile(getFile(dataFilePath));
    }
    
    public void loadFile(File file) {
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file));
        
             while(bf.ready()) {
                 String[] split = bf.readLine().split(",");
                 
                 // If the array does not contain all the values
                 if(split.length < 34) continue;
                 
                 String isoCode = split[0];
                 String continent = split[1];
                 String location = split[2];
                 String date = split[3];
                 double totalCases = (!split[4].isEmpty() ? Double.valueOf(split[4]) : 0.0);
                 double totalDeaths = (!split[7].isEmpty() ? Double.valueOf(split[7]) : 0.0);
                 double totalVaccinations = (!split[34].isEmpty() ? Double.valueOf(split[34]) : 0.0);
                 double fullyVaccinated = (!split[36].isEmpty() ? Double.valueOf(split[36]) : 0.0);
                 double totalTests = (!split[26].isEmpty() ? Double.valueOf(split[26]) : 0.0);
                 double totalHospitalizations = (!split[19].isEmpty() ? Double.valueOf(split[19]) : 0.0);
                 double positiveRate = (!split[31].isEmpty() ? Double.valueOf(split[31]) : 0.0);
           
                 // Adds the point to the isocode list
                 DataPoint dataPoint = new DataPoint(isoCode, continent, location, date, totalCases, totalDeaths, totalVaccinations, fullyVaccinated, totalTests, totalHospitalizations, positiveRate);
                 if(getPoints().containsKey(isoCode)) {
                     
                     // Makes the graph smooth, stops there from being points that have <= 0 values
                     ArrayList<DataPoint> pointsForIsoCode = getPoints().get(isoCode);
                     DataPoint priorPoint = pointsForIsoCode.get(pointsForIsoCode.size()-1);
                     
                     if(priorPoint.getTotalCases() >= 0 && totalCases <= 0) continue;
                     if(priorPoint.getTotalDeaths()>= 0 && totalDeaths <= 0) continue;
                     if(priorPoint.getTotalVaccinations()>= 0 && totalVaccinations <= 0) continue;
                     //if(priorPoint.getTotalHospitalization()>= 0 && totalHospitalizations <= 0) continue;
                     //if(priorPoint.getFullyVaccinated()>= 0 && fullyVaccinated <= 0) continue;
                     //if(priorPoint.getTotalTests()>= 0 && totalTests <= 0) continue;
                     //if(priorPoint.getPositiveRate()>= 0 && positiveRate <= 0) continue;


                     // Adds the point.
                     getPoints().get(isoCode).add(dataPoint);
                 } 
                 // Adds the lsit if it does not exist
                 else {
                     ArrayList<DataPoint> list = new ArrayList<>();
                     list.add(dataPoint);
                     // Adds list to the set of points
                     getPoints().put(isoCode, list);
                 }
             }
             
        } catch(IOException e) {
            System.out.println("File is not found.");
        }
    }
    
    /**
     * Prints all the data out to the console
     */
    public void printAllData() {
        for(ArrayList<DataPoint> n : getPoints().values()) {
            for(DataPoint m : n) {
                System.out.println(m);
            }
        }
    }

    public DataManager(HashMap<String, ArrayList<DataPoint>> points) {
        this.points = points;
    }
    
    /**
     * Gets points from an ISO Code
     * 
     * @param isoCode String code to be entered
     * @return A list of DataPoints associated with the given isoCode.
     */
    public ArrayList<DataPoint> getPoints(String isoCode) {
        return points.get(isoCode);
    }
    
    /**
     * @return the points
     */
    public HashMap<String, ArrayList<DataPoint>> getPoints() {
        return points;
    }
    
    /**
     * Private methods
     */
    
    /**
     * Finds a file from a path
     * 
     * @param path Location of the file
     * @return File object
     */
    private File getFile(String path) {  
      File file = new File(path);        
      return file.exists() ? file : null;         
    }
    
}
