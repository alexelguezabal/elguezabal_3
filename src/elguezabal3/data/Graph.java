/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.data;

import java.awt.Color;
import java.awt.Image;
import java.awt.Paint;
import java.util.List;
import java.util.TimeZone;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.Range;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 * Holds a graph
 * @author Alex
 */
public class Graph {
  
    private List<DataPoint> points;
    private Info info;
    private Color color;
    private Image backgroundImage;
    
    private JFreeChart jFreeChart;
    private TimeTableXYDataset dataset;
    
    /**
     * Default constructor 
     * 
     * @param points
     * @param info
     * @param color 
     * @param backgroundImage 
     */
    public Graph(List<DataPoint> points, Info info, Color color, Image backgroundImage) {
        this.points = points;
        this.info = info;
        this.color = color;
        this.backgroundImage = backgroundImage;
        
        // Creates the jfreechart
        cheateChart();
    }
       
    /**
     * Creates a Tooltip.
     * ToDo
     * 
     * @param x
     * @param y 
     */
    public void getToolTip(int x, int y) {
        XYToolTipGenerator tooltip = jFreeChart.getXYPlot().getRenderer().getToolTipGenerator(x, y);
        
        //System.out.println(tooltip.generateToolTip(dataset, 0, x));
    }  
    
    
    /**
     * @return the jFreeChart
     */
    public JFreeChart getjFreeChart() {
        return jFreeChart;
    }

    /**
     * @return the points
     */
    public List<DataPoint> getPoints() {
        return points;
    }

    /**
     * @return the info
     */
    public Info getInfo() {
        return info;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Color color) {
        this.color = color;
        
        // Updates the color of the current JFreeChart
        this.jFreeChart.getXYPlot().getRenderer().setSeriesPaint(0, getColor());
        
        // Updates the range crosshaircolor
        jFreeChart.getXYPlot().setDomainMinorGridlinePaint(getColor());
    }

    /**
     * @return the backgroundImage
     */
    public Image getBackgroundImage() {
        return backgroundImage;
    }

    /**
     * @param backgroundImage the backgroundImage to set
     */
    public void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
        
        // Updates the background image
        if(getBackgroundImage() != null){
            jFreeChart.setBackgroundImage(getBackgroundImage());
        } 
    }
    
    /**
     * Returns an image of this graph
     * 
     * @return A Buffered Image of this graph.
     */
    public Image getImage() {
        return jFreeChart.createBufferedImage(700, 500);
    }
    
    
    /**
     * Private methods
     */
    
    /**
     * Creates this classes JFreeChart
     */
    private void cheateChart() {
        // Creating the XYData Set
        dataset = new TimeTableXYDataset(TimeZone.getDefault());
       
        // Adds all the points
        getPoints().forEach(n -> {   
            dataset.add(n.getDay(), n.getFromInfo(getInfo()), "Date");
        });
        
        // Creating the TimeSeriesChart
        jFreeChart = ChartFactory.createTimeSeriesChart(getInfo().getBoxSelectionName(), "Date", getInfo().getBoxSelectionName(), dataset, true, true, true);
        jFreeChart.getXYPlot().getRenderer().setSeriesPaint(0, getColor());
        
        XYToolTipGenerator xyToolTipGenerator = (XYDataset dataset1, int series, int item) -> {
            Number x1 = dataset1.getX(series, item);
            Number y1 = dataset1.getY(series, item);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(String.format("<html><p style='color:#0000ff;'>Serie: '%s'</p>", dataset1.getSeriesKey(series)));
            stringBuilder.append(String.format("X:'%d'<br/>", x1.intValue()));
            stringBuilder.append(String.format("Y:'%d'", y1.intValue()));
            stringBuilder.append("</html>");
            return stringBuilder.toString();
        };

        jFreeChart.getXYPlot().getRenderer().setBaseToolTipGenerator(xyToolTipGenerator);
        
        if(getBackgroundImage() != null){
            jFreeChart.setBackgroundImage(getBackgroundImage());
        }
        
        // Title of the Panel
        jFreeChart.setTitle(new TextTitle("Covid-19 Information for "+points.get(0).getLocation()));
        
        // Removes Scientific Notation
        jFreeChart.getXYPlot().getRangeAxis().setAutoRangeMinimumSize(100);
        
        // Assers that if this graph is a positive rate that the range will stay fixed 0 -> 1
        if(info == Info.POSITIVE_RATE) {
            jFreeChart.getXYPlot().getRangeAxis().setRange(new Range(0.0, 1.0));
        }
    }        
}
