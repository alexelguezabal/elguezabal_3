/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.data;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author Alex
 */
public enum Info {
    
    TOTAL_CASES("Total Cases"),
    TOTAL_DEATHS("Total Deaths"),
    TOTAL_VACCINATIONS("Total Vaccinations"),
    FULLY_VACCINATED("Fully Vaccinated People"),
    TOTAL_TESTS("Total Tests"),
    TOTAL_HOSPITALIZATION("Total Hospitalization"),
    POSITIVE_RATE("Positive Rate");
        
    public String boxSelectionName;
    
    Info(String string) {
        this.boxSelectionName = string;
    }
    
    /**
     * Gets an info by the name
     * 
     * @param name String to be checked
     * @return The Info of the type
     */
    public static Info getByName(String name) {
        return Arrays.stream(Info.values()).filter(n -> n.getBoxSelectionName().equals(name)).findFirst().orElse(null);
    }
    
    /**
     * Gets the box names of this class
     * 
     * @return Object[] of the names
     */
    public static Object[] boxNames() {
        return Arrays.stream(Info.values()).map(n -> n.getBoxSelectionName()).collect(Collectors.toList()).toArray();
    }

    /**
     * @return the boxSelectionName
     */
    public String getBoxSelectionName() {
        return boxSelectionName;
    }    
    
}
