/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.data;

import com.jidesoft.plaf.basic.BasicStyledLabelUI;
import java.util.Date;
import org.jfree.data.time.Day;

/**
 * Represents a point of data 
 * Used for graphing
 * 
 * @author Alex
 */
public class DataPoint {
    
    /*
    Breakdown of file seperation
    iso_code,continent,location,date,total_cases,
    new_cases,new_cases_smoothed,total_deaths,
    new_deaths,new_deaths_smoothed,total_cases_per_million,
    new_cases_per_million,new_cases_smoothed_per_million,
    total_deaths_per_million,new_deaths_per_million,new_deaths_smoothed_per_million,
    reproduction_rate,icu_patients,icu_patients_per_million,hosp_patients,hosp_patients_per_million,
    weekly_icu_admissions,weekly_icu_admissions_per_million,weekly_hosp_admissions,weekly_hosp_admissions_per_million,
    new_tests,total_tests,total_tests_per_thousand,new_tests_per_thousand,new_tests_smoothed,new_tests_smoothed_per_thousand,
    positive_rate,tests_per_case,tests_units,total_vaccinations,people_vaccinated,people_fully_vaccinated,total_boosters,new_vaccinations,
    new_vaccinations_smoothed,total_vaccinations_per_hundred,people_vaccinated_per_hundred,people_fully_vaccinated_per_hundred,total_boosters_per_hundred,
    new_vaccinations_smoothed_per_million,new_people_vaccinated_smoothed,new_people_vaccinated_smoothed_per_hundred,stringency_index,population,population_density,
    median_age,aged_65_older,aged_70_older,gdp_per_capita,extreme_poverty,cardiovasc_death_rate,diabetes_prevalence,female_smokers,male_smokers,handwashing_facilities,
    hospital_beds_per_thousand,life_expectancy,human_development_index,excess_mortality_cumulative_absolute,excess_mortality_cumulative,excess_mortality,
    excess_mortality_cumulative_per_million
    */
    
    private String isoCode;
    private String continent;
    private String location;
    private String date;
    
    private double totalCases;
    private double totalDeaths;
    private double totalVaccinations;
    private double fullyVaccinated;
    private double totalTests;
    private double totalHospitalization;
    private double positiveRate;
    
    
    /**
     * Constructor for a Data Point
     * 
     * @param isoCode
     * @param cotinent
     * @param location
     * @param date
     * @param totalCases
     * @param totalDeaths
     * @param totalVaccinations 
     */
    public DataPoint(String isoCode, String cotinent, String location, String date, double totalCases, double totalDeaths, double totalVaccinations, double fullyVaccinated, double totalTests, double totalHospitalization, double positiveRate) {
        this.isoCode = isoCode;
        this.continent = cotinent;
        this.location = location;
        this.date = date;
        this.totalCases = totalCases;
        this.totalDeaths = totalDeaths;
        this.totalVaccinations = totalVaccinations;
        this.fullyVaccinated = fullyVaccinated;
        this.totalTests = totalTests;
        this.totalHospitalization = totalHospitalization;
        this.positiveRate = positiveRate;
    }
        
    /**
     * Gets the info of this class from an Info object
     * 
     * @param info Information to get
     * @return Info object
     */
    public double getFromInfo(Info info) {
        switch(info) {
            case TOTAL_CASES: return totalCases;
            case TOTAL_DEATHS: return totalDeaths;
            case TOTAL_VACCINATIONS: return totalVaccinations;
            case FULLY_VACCINATED: return getFullyVaccinated();
            case TOTAL_HOSPITALIZATION: return getTotalHospitalization();
            case TOTAL_TESTS: return getTotalTests();
            case POSITIVE_RATE: return getPositiveRate();
        }
        
        return 0.0;
    }
    
    /**
     * ToString methods
     * 
     * @return Prints out the info of this datapoint
     */
    @Override
    public String toString() {
        return isoCode 
                + " " + continent 
                + " " + location 
                + " " + date 
                + " " + totalCases 
                + " " + totalDeaths 
                + " " + totalVaccinations 
                + " " + getFullyVaccinated() 
                + " " + getTotalTests() 
                + " " + getTotalHospitalization() 
                + " " + getPositiveRate();
    }
    
        /**
     * @return the isoCode
     */
    public String getIsoCode() {
        return isoCode;
    }

    /**
     * @return the continent
     */
    public String getContinent() {
        return continent;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @return the totalCases
     */
    public double getTotalCases() {
        return totalCases;
    }

    /**
     * @return the totalDeaths
     */
    public double getTotalDeaths() {
        return totalDeaths;
    }
    
    /**
     * Creates a Day from an String
     * @return 
     */
    public Day getDay() {
        String[] splitDate = date.split("-");
        // Creates the day object based of the split day values
        Day day = new Day(Integer.valueOf(splitDate[2]), Integer.valueOf(splitDate[1]), Integer.valueOf(splitDate[0]));
               
        return day;
    }

    /**
     * @return the totalVaccinations
     */
    public double getTotalVaccinations() {
        return totalVaccinations;
    }

    /**
     * @return the fullyVaccinated
     */
    public double getFullyVaccinated() {
        return fullyVaccinated;
    }

    /**
     * @return the totalTests
     */
    public double getTotalTests() {
        return totalTests;
    }

    /**
     * @return the totalHospitalization
     */
    public double getTotalHospitalization() {
        return totalHospitalization;
    }

    /**
     * @return the positiveRate
     */
    public double getPositiveRate() {
        return positiveRate;
    }
    
}
