/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package elguezabal3.data;

import elguezabal3.Main;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Alex
 */
public class GraphManager {
    
    private Graph currentGraph;
    private Graph currentGraphToDisplay;
    private Color currentColor;
    
    private Image currentBackgroundImage;
    
    /**
     * Default Constructor
     */
    public GraphManager() {
        this.currentGraph = createGraph(Main.getDataManager().getPoints("ABW"), Info.TOTAL_CASES);
        this.currentGraphToDisplay = currentGraph;
        this.currentColor = Color.BLACK;
        this.currentBackgroundImage = null;
    }
    
    /**
     * Creates a graph 
     * 
     * @param points List of the points
     * @param info Info type
     * @return A graph
     */
    public Graph createGraph(List<DataPoint> points, Info info) {
        return createGraph(points, info, getCurrentColor(), getCurrentBackgroundImage());
    }
    
    /**
     * Creates a graph 
     * 
     * @param points List of the points
     * @param info Info type
     * @param color Color of the graph
     * @param image Background of the image
     * @return A graph
     */
    public Graph createGraph(List<DataPoint> points, Info info, Color color, Image image) {
        return new Graph(points, info, color, image);
    }
    
    /**
     * Displays a temporary graph without assigning the new graph to the curent graph
     * 
     * @param points List of the points
     * @param info Info type
     * @param color Color of the graph
     */
    public void displayTemporaryGraph(List<DataPoint> points, Info info, Color color) {
        this.currentGraphToDisplay = createGraph(points, info, color, getCurrentBackgroundImage());
        Main.getFrame().getGraphPanel().displayGraph();
    }

    /**
     * @return the currentGraph
     */
    public Graph getCurrentGraph() {
        return currentGraph;
    }

    /**
     * @param currentGraph the currentGraph to update
     */
    public void updateCurrentGraph(Graph currentGraph) {
        this.currentGraph = currentGraph;
        this.currentGraphToDisplay = currentGraph;
    }
    
    /**
     * @return the currentGraphToDisplay
     */
    public Graph getCurrentGraphToDisplay() {
        return currentGraphToDisplay;
    }

    /**
     * @return the currentColor
     */
    public Color getCurrentColor() {
        return currentColor;
    }

    /**
     * @param currentColor the currentColor to set
     */
    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }
    
    /**
     * Updates the color of the current graph
     */
    public void updateColorOfGraph() {
        // Updates the current color on settings panel
        Main.getFrame().getSettingsPanel().updateCurrentColorLabel(true);
        
        // Updates the current graph to be the selected color
        this.currentGraph.setColor(this.currentColor);
        this.currentGraphToDisplay.setColor(this.currentColor);
        
        // Updates the graph on screen
        Main.getFrame().getGraphPanel().repaint();
    }

    /**
     * @return the currentBackgroundImage
     */
    public Image getCurrentBackgroundImage() {
        return currentBackgroundImage;
    }

    /**
     * @param currentBackgroundImage the currentBackgroundImage to set
     */
    public void setCurrentBackgroundImage(Image currentBackgroundImage) {
        this.currentBackgroundImage = currentBackgroundImage;
        
        // Updates the graphs background image
        this.currentGraph.setBackgroundImage(this.currentBackgroundImage);
        this.currentGraphToDisplay.setBackgroundImage(this.currentBackgroundImage);
        
        // Updates the graph on screen
        Main.getFrame().getGraphPanel().repaint();
    }
        
     /**
     * Prompts the user to save the screened graph
     * Uses JFileChooser, haulting any GUI interaction.
     */
     public void saveCurrentGraph() {
        
        // Gets the image to save
        Graph graph = currentGraphToDisplay;
        if(graph == null) {JOptionPane.showMessageDialog(null, "You have no image loaded to save."); return;}
        Image image = graph.getImage();
        if(image == null) {JOptionPane.showMessageDialog(null, "You have no image loaded to save."); return;}
        
        File file = null;
        
        // (! https://www.codejava.net/java-se/swing/show-save-file-dialog-using-jfilechooser)  
        JFileChooser jfc = new JFileChooser(System.getProperty("user.home") + "/Pictures");
        jfc.setDialogTitle("Save Graph");
        
        int returnValue = jfc.showSaveDialog(Main.getFrame());

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            file = jfc.getSelectedFile();
            
            // Determines if the file selected is an image, if not an erro is displayed.
            if (!file.getPath().endsWith(".png") && !file.getPath().endsWith(".jpg")) {
                JOptionPane.showMessageDialog(null, "A incorrect file-type was selected, please only chose PNGs and JPGs.");
                return;
            }
        }  
        // Displays an error if no file was selected
        else {
            JOptionPane.showMessageDialog(null, "You did not select a file");
            return;
        }
        
        // Check to see if the file already exists
        if(file.exists()) {
            // Prompt the user if they want to override.
            int value = JOptionPane.showConfirmDialog(null, "This File already exists, would you like to override it?", "File Exists", JOptionPane.YES_NO_OPTION);
            if(value != 0) return;
            
            // Delets files
            file.delete();
        }
        
        String type = (file.getPath().endsWith(".png") ? "png" : "jpg"); 
        
       // Writing the Image out
       try {
           ImageIO.write((BufferedImage) image, type, file);
       } catch (IOException e) {
           e.printStackTrace();
       }   
    }
    
}
